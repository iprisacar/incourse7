const N = 9;
let snake = [
    [5,4],
    [6,4],
    [7,4],
];
let lastMove = 'up';

let snakeHtml = document.getElementById('snake');

function draw(){
    let html = '';
    for (var y = 0; y < N; y++) {
        html += '<div class="row">';
            for (var x = 0; x < N; x++) {
                let snakeClass = '';
                for (var s = 0; s < snake.length; s++) {
                    if(snake[s][0] == y && snake[s][1] == x){
                        snakeClass = 'snake';
                        break;
                    }
                }
                html += '<div x="'+x+'" y="'+y+'" class="box '+snakeClass+'"></div>';
            }
        html += '</div>';
    }
    snakeHtml.innerHTML = html;
}

function preDraw(newBox){
    snake.unshift(newBox);
    snake.pop();
    draw();
}

function left(){
    let firstBox = cloneBox(snake[0]);
    firstBox[1] = firstBox[1] - 1 < 0 ? N - 1 : --firstBox[1];
    preDraw(firstBox);
}

function up(){
    let firstBox = cloneBox(snake[0]);
    firstBox[0] = firstBox[0] - 1 < 0 ? N - 1 : --firstBox[0];
    preDraw(firstBox);
}

function right(){
    let firstBox = cloneBox(snake[0]);
    firstBox[1] = firstBox[1] + 1 >= N ? 0 : ++firstBox[1];
    preDraw(firstBox);
}

function down(){
    let firstBox = cloneBox(snake[0]);
    firstBox[0] = firstBox[0] + 1 >= N ? 0 : ++firstBox[0];
    preDraw(firstBox);
}

function cloneBox(box){
    let newBox = [];
    newBox.push(box[0]);
    newBox.push(box[1]);
    return newBox;
}

draw();

window.addEventListener('keyup', move);

function move(e){
    switch (e.keyCode) {
        case 37:
            if(lastMove !== 'right'){
                lastMove = 'left';
                left();
            }
            break;
        case 38:
            if(lastMove !== 'down'){
                lastMove = 'up';
                up();
            }
            break;
        case 39:
            if(lastMove !== 'left'){
                lastMove = 'right';
                right();
            }
            break;
        case 40:
            if(lastMove !== 'up'){
                lastMove = 'down';
                down();
            }
            break;
    }
}
