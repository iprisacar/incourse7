const images= ['img/1.jpg','img/2.jpg','img/3.jpg'];
let prev = document.getElementById("prev");
prev.addEventListener("click", prevSlide);
let next = document.getElementById("next");
next.addEventListener("click", nextSlide);
let index= 0;

function prevSlide() {
    index--;
    if(index<0){
        index= images.length-1;
    }
    document.getElementById("slider").src=images[index];
}
function nextSlide() {
    index++;
    if(index>images.length-1){
        index= 0;
    }
    document.getElementById("slider").src=images[index];
}
