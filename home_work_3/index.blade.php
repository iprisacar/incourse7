@extends('layouts.app')

@section('title', 'Пости')
@section('body')

    <div class="container">
        @include('includes.header')
        <h1 align="center">Авторизація</h1>
        <form>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="form-group ">
                         <label for="exampleInputEmail1">Ваша почтова скринька</label>
                        <input required type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group ">
                         <label for="exampleInputPassword1">Пароль</label>
                         <input required type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-check">
                         <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Чужий комп'ютер</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Війти</button>
                </div>
            </div>
        </form>
        <div class="col-md-4"></div>

    </div>
@endsection
