@extends('layouts.app')

@section('title', 'Пользователи')

@section('body')
    <div class="container">
        @include('includes.header')
        <h1 align="center">Регистрація</h1>
        <form>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Як вас звати?</label>
                    <input  class="form-control"  placeholder="Ім'я">
                </div>
                <div class="form-group col-md-6">
                    <label>Ваше прізвище</label>
                    <input  class="form-control"  placeholder="Прізвище">
                </div>
                <div class="form-group col-md-12">
                     <label>Поштова скринька</label>
                    <input required  class="form-control" type="email"  placeholder="Email">
                </div>
                <div class="form-group col-md-6">
                    <label>Придумайте пароль</label>
                    <input required class="form-control"  placeholder="Пароль">
                </div>
                <div class="form-group col-md-6">
                    <label>Підтвердіть пароль</label>
                    <input required class="form-control" placeholder="Пароль">
                </div>
                <div class="form-group col-md-4">
                     <label for="inputAddress">Дата народження</label>
                     <input type="text" class="form-control" placeholder="Число">
                </div>
                <div class="form-group col-md-4">
                    <label>Місяць</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>Рік</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group col-md-6">
                <select required class="custom-select">
                    <option value="1">Батьки</option>
                    <option value="2">5 клас</option>
                    <option value="3">6 клас</option>
                </select>
                </div>
                <div class="form-group col-md-6">
                   <label>Стать</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline1">Чоловіча</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline2">Жіноча</label>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Реєстрація</button>
        </form>
    </div>
@endsection
